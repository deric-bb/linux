#!/bin/bash
# Bash Menu Script Example

PS3='Hi, how can I help you? Please enter a choice 1-3: '
options=("Date" "To-Dos" "Reminders" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Date")
            echo "The date today is: "
            date
            ;;
        "To-Dos")
            echo "Here is your most recent to-do list"
            cat to-do-list.txt
            ;;
        "Reminders")
            echo "Here is your reminder list: "
            cat reminders.txt
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
