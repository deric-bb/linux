#!/bin/bash

# Check website for update with mention of David's name
# Continue checking until the update has been made and then execute the email script 


until curl https://jdgite.wixsite.com/my-site | grep "David"
do
   echo "Waiting for the text in the site"
   sleep 1s
done

echo "Found it"
date

#Build email notification 
./email.sh

