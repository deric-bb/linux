To add files to the bitbucket repo, follow the steps below

Create your files or modify existing files

Create a new branch for the changes
git checkout -b <branch name>
ex: git checkout -b branch1

Add the files to be uploaded
git add <file name>
ex: git add script.sh

Add a commit message:
git commit -m "Added new script"

Push the changes to the repo:
git push

Change back to the master branch
git checkout master


** For first time commits run this first:
git config --global user.email "ramsey29797@gmail.com"
